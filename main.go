package main

import "gitlab.com/scgodbold/golang-pantry/cmd"

func main() {
	cmd.Execute()
}
