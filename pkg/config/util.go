package config

import (
	"os"

	"path/filepath"
)

func resolvePath(path string) string {
	expandPath := os.ExpandEnv(path)
	evaledPath, err := filepath.EvalSymlinks(expandPath)
	if err != nil {
		return expandPath
	}
	return evaledPath
}
