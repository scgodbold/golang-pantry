package config

import (
	"os"
	"strings"

	"github.com/coreos/pkg/capnslog"
	"github.com/spf13/viper"
)

var log = capnslog.NewPackageLogger("gitlab.com/scgodbold/golang-pantry", "config")

func init() {
	capnslog.SetFormatter(capnslog.NewPrettyFormatter(os.Stdout, false))
	capnslog.SetGlobalLogLevel(capnslog.WARNING)

	viper.SetEnvPrefix("pantry")
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")
	viper.AddConfigPath(resolvePath("$HOME/.config/pantry"))
	viper.AddConfigPath(".")
	viper.AutomaticEnv()

	// Attempt to setup config at runtime
	// Pass on errors as it doesnt matter if its not found
	// Any config passed in will trample these settings???
	err := viper.ReadInConfig()

	// Set some default configuration
	viper.Set("pantrymanifestdir", resolvePath("$HOME/.config/pantry/manifest"))
	viper.Set("pantrydir", resolvePath("$HOME/pantry"))
	viper.Set("loglevel", "Warning")

	err = viper.ReadInConfig()
	if err != nil {
		e, ok := err.(viper.ConfigParseError)
		if ok {
			log.Fatalf("Trouble parsing config: %v", e)
		}
		log.Debug("No config found")
	} else {
		log.Debugf("Using config file: %v", viper.ConfigFileUsed())
	}

}

func setLogLevel(logLevel string) error {
	lvl, err := capnslog.ParseLevel(strings.ToUpper(logLevel))
	if err != nil {
		log.Warningf("Invalid loglevel %v", logLevel)
		return err
	}
	capnslog.SetGlobalLogLevel(lvl)
	return nil
}

func OnInitialize(logLevel string) error {
	err := viper.ReadInConfig()
	if err != nil {
		e, ok := err.(viper.ConfigParseError)
		if ok {
			log.Fatalf("Trouble parsing config: %v", e)
		}
		log.Debug("No config found")
	} else {
		log.Debugf("Using config file: %v", viper.ConfigFileUsed())
	}

	if logLevel != "" {
		setLogLevel(logLevel)
	} else if viper.Get("logLevel") != nil {
		setLogLevel(logLevel)
	}

	return nil
}
