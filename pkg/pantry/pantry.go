package pantry

import (
	"fmt"
	"os"

	"github.com/coreos/pkg/capnslog"
)

var log = capnslog.NewPackageLogger("gitlab.com/scgodbold/golang-pantry", "pantry")

var pantries map[string]*Pantry

type Pantry struct {
	Name           string `yaml:"name"`
	definitionPath string
	PantryPath     string   `yaml:"path"`
	Repos          []string `yaml:"repos"`
}

func Exists(name string) bool {
	if _, ok := pantries[name]; ok {
		return true
	}
	return false
}

// Factory to create new pantries
// Writes a Manifest and then creates
// the actual Directory
func New(name string) (Pantry, error) {
	var p Pantry
	defPath := fmt.Sprintf("%v/%v.yaml", manifestDir, name)
	file, err := os.Create(defPath)
	defer file.Close()
	if err != nil {
		log.Errorf("Encountered an error creating a new pantry: %v", err)
		return p, err
	}
	p.Name = name
	p.PantryPath = fmt.Sprintf("%v/%v", pantryDir, name)
	if err = os.MkdirAll(p.PantryPath, 0755); err != nil {
		log.Errorf("Encountered an error creating a new pantry: %v", err)
		return p, err
	}
	writeManifest(file, p)
	p.definitionPath = defPath

	return p, nil
}

func init() {
	pantries = make(map[string]*Pantry)
}
