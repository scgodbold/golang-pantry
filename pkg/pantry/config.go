package pantry

import (
	"io"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

var pantryDir string
var manifestDir string

func SetPantryDir(dir string) error {
	log.Infof("Setting pantry directory to: %v", dir)
	var err error
	pantryDir, err = filepath.Abs(dir)
	if err != nil {
		return err
	}
	return nil
}

func SetManifestDir(dir string) error {
	log.Infof("Setting manifest directory to: %v", dir)
	var err error
	manifestDir, err = filepath.Abs(dir)
	if err != nil {
		return err
	}

	err = filepath.Walk(manifestDir, processManifests)
	return err
}

func processManifests(path string, info os.FileInfo, err error) error {
	if info.IsDir() {
		return nil
	}
	extension := filepath.Ext(path)
	if extension != ".yaml" && extension != ".yml" {
		return nil
	}
	log.Debugf("Proccessing Manifest %v", path)
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return err
	}
	return proccessManifest(file, path)
}

func proccessManifest(r io.Reader, path string) error {
	decoder := yaml.NewDecoder(r)
	var p Pantry
	if err := decoder.Decode(&p); err != nil {
		return err
	}
	p.definitionPath = path
	pantries[p.Name] = &p
	return nil
}

func writeManifest(w io.Writer, p Pantry) error {
	encoder := yaml.NewEncoder(w)
	defer encoder.Close()
	err := encoder.Encode(p)

	return err
}
