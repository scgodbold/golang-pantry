package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

const (
	version = "0.0.1"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Prints pantry version",
	Long:  "Prints pantry version",
	Run: func(c *cobra.Command, args []string) {
		fmt.Println("pantry version " + version)
	},
}

func init() {
	RootCmd.AddCommand(versionCmd)
}
