package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/scgodbold/golang-pantry/pkg/pantry"
)

var CreatePath string

var createCmd = &cobra.Command{
	Use:   "create [name]",
	Short: "Creates a new pantry",
	Long:  "Creates a new pantry",
	Args:  createArgsFunc, Run: createFunc,
}

func createArgsFunc(cmd *cobra.Command, args []string) error {
	if len(args) < 1 {
		return errors.New("Must provide a name to create a new pantry")
	}
	if !pantry.Exists(args[0]) {
		return nil
	}
	return fmt.Errorf("%v pantry already exists", args[0])
}

func createFunc(c *cobra.Command, args []string) {
	name := args[0]
	log.Debugf("Attempting to create pantry: %v", name)
	if CreatePath != "" {
		pantry.SetPantryDir(CreatePath)
	}
	pantry.New(name)
	return
}

func init() {
	createCmd.Flags().StringVarP(&CreatePath, "path", "p", "", "Path for pantry to be located")
	RootCmd.AddCommand(createCmd)
}
