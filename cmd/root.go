package cmd

import (
	"fmt"
	"os"

	"github.com/coreos/pkg/capnslog"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/scgodbold/golang-pantry/pkg/config"
	"gitlab.com/scgodbold/golang-pantry/pkg/pantry"
)

var logLevel string

var log = capnslog.NewPackageLogger("gitlab.com/scgodbold/golang-pantry", "cmd")

var RootCmd = &cobra.Command{
	Use:   "pantry",
	Short: "Gather and manage like minded repos into a workspace",
	Long:  "",
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println("Error: err")
		os.Exit(1)
	}
}

func init() {
	RootCmd.PersistentFlags().StringVar(&logLevel, "log-level", "", "log level [Panic,Fatal,Error,Warn,Info,Debug]")
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	config.OnInitialize(logLevel)
	pantry.SetPantryDir(viper.GetString("pantrydir"))
	pantry.SetManifestDir(viper.GetString("pantrymanifestdir"))
}
